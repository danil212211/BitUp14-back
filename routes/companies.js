var express = require('express');
var router = express.Router();
var database = require("../modules/db.js");
let db= database.db;

const multer= require("multer");
const path = require('path');
function isEmptyObject(obj) {
    return !Object.keys(obj).length;
}
function checkExist(a) {
    return typeof a !== "undefined" && a !== null;
}
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'C:/Projects_Hackaton/BitUp14-front/assets/companies')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname) //Appending extension
    }
});

const fileUpload=multer({ storage : storage});
/* GET home page. */
router.get('/',async function(req, res) {

    let data=req.query;
    let offset=0;
    if (checkExist(data.offset)) offset=data.offset;
    let limit=50;
    if ( checkExist(data.limit)) limit=data.limit;
    if (checkExist(data.admin_id)) {
        let adminId=data.admin_id;
        let adminComp= await db.query(`SELECT * FROM companies WHERE admin_id=${adminId}`);
        console.log(adminComp);
        res.status(200).json(adminComp);
        return ;
    }
    if (checkExist(data.companyId)) {
        let company=await db.query(`SELECT * FROM companies WHERE id=${data.companyId}`);
        res.status(200).json(company);
        return ;
    }
    let posts= await db.query(`SELECT * FROM companies LIMIT ${offset} , ${limit}   `);
    res.status(200).json(posts);
    return ;
});
router.post('/',fileUpload.single('image'),async (req,res)=> {
   let data = req.body;
   let imageName=req.file.filename;
   let adminId=data.uid;
   let companyName=data.name;
   await db.query(`INSERT INTO companies (admin_id , name, image) VALUES ( '${adminId}' , '${companyName}' , '${imageName}')`);
   res.status(200).json({});
});

module.exports = router;
