var express = require('express');
var router = express.Router();
var database = require("../modules/db.js");
let db= database.db;

const multer= require("multer");
const path = require('path');
function isEmptyObject(obj) {
    return !Object.keys(obj).length;
}
function checkExist(a) {
    return typeof a !== "undefined" && a !== null;
}
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'C:/Projects_Hackaton/BitUp14-front/assets/chat')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname) //Appending extension
    }
});
const fileUpload=multer({ storage : storage});
router.get("/",async (req,res)=> {

    let data=req.query;
    let from=data.fromId;
    let to=data.toId;
    let offset=0;
    if (checkExist(data.offset)) offset=data.offset;
    let limit=50;
    if ( checkExist(data.limit)) limit=data.limit;
    console.log(from + " " + to);
    if (checkExist(from) && checkExist(to) && !isNaN(to) && !isNaN(from) && to!==null && from!==null) {
        let chat=await   db.query(`SELECT * FROM chat WHERE user_id=${from} AND company_id=${to}`);
        if (isEmptyObject(chat)) {
            chat=await   db.query(`SELECT * FROM chat WHERE user_id=${to} AND company_id=${from}`);
        }
        if (isEmptyObject(chat)) {
            res.status(200).json({});
            return ;
        }
        let id=chat[0].id;
        let messages=await db.query(`SELECT * FROM chat_messages WHERE chat_id=${id}`);
        console.log(messages);
        res.status(200).json(messages);
        return ;
    }
    res.status(400).json({});
    return ;
});
module.exports=router;