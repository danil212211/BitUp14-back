var express = require('express');
var router = express.Router();
var database = require("../modules/db.js");
let db= database.db;

const multer= require("multer");
const path = require('path');
function isEmptyObject(obj) {
    return !Object.keys(obj).length;
}
function checkExist(a) {
    return typeof a !== "undefined" && a !== null;
}
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'C:/Projects_Hackaton/BitUp14-front/assets/achievements')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname) //Appending extension
    }
});
const fileUpload= multer( {storage : storage});
router.get('/',async (req,res)=> {
    let userId=req.query.uid;
    if (checkExist(userId)) {
        let needs = await db.query(`SELECT * FROM achievements WHERE user_id=${userId}`);
        res.status(200).json(needs);
    }
});
router.post('/',fileUpload.single('image'),async (req,res)=> {
   let data=req.body;
   let userId=data.uid;
   let imageName=req.file.filename;
   let ans=await db.query(`INSERT INTO achievements (user_id , image) VALUES ('${userId}', '${imageName}')`);
   let image=await db.query(`SELECT * FROM achievements WHERE id=${ans.insertId}`);
   res.status(200).json(image);
});
module.exports = router;