var express = require('express');
var router = express.Router();
var database = require("../modules/db.js");
let db= database.db;

const multer= require("multer");
const path = require('path');
function isEmptyObject(obj) {
    return !Object.keys(obj).length;
}
function checkExist(a) {
    return typeof a !== "undefined" && a !== null;
}

router.get('/',async (req,res)=> {
    let data=req.query;
    console.log("peworprwoe " + data.eventId );
    if (checkExist(data.eventId)) {
        let eventId=data.eventId;
        let connections= await db.query(`SELECT * FROM company_connections WHERE event_id=${eventId}`);
        let arr=[];
        for (let i=0;i<connections.length;++i) {
            let company=await db.query(`SELECT * FROM companies WHERE id=${connections[i].company_from}`);
            let needing= await db.query(`SELECT * FROM needs WHERE id=${connections[i].needing_id}`);
            let exist_id=arr.findIndex(element => element.needing_id===needing[0].id);
            if (exist_id!==-1) {
                arr[exist_id].companies.push(company[0]);
            } else {
                arr.push({
                    needing_id : needing[0].id,
                    needing_name : needing[0].needing,
                    companies : [company[0]]
                })
            }
        }
        res.status(200).json(arr);
        return ;
    }
    let needs=await db.query(`SELECT * FROM company_connections `);
    res.status(200).json(needs);
});
module.exports =router;