var express = require('express');
var router = express.Router();
var database = require("../modules/db.js");
let db= database.db;

const multer= require("multer");
const path = require('path');
function isEmptyObject(obj) {
    return !Object.keys(obj).length;
}
function checkExist(a) {
    return typeof a !== "undefined" && a !== null;
}

router.post('/',async (req,res) => {
    let data=req.body;
    let userId=data.userId;
    let postId=data.postId;
    if (checkExist(userId) && checkExist(postId)) {
        let isExist = await db.query(`SELECT * FROM post_likes WHERE user_id=${userId} AND post_id=${postId}`);
        if (isEmptyObject(isExist)) {
            let ans= await db.query(`INSERT INTO post_likes (user_id , post_id) VALUES ('${userId}' , '${postId}')`);
            res.status(200).json(ans);
        }
        return ;
    }
    res.status(400).json({});
});
router.delete('/',async(req,res)=> {
    let data=req.query;
    console.log(data);
    let userId=data.userId;
    let postId=data.postId;
    if (checkExist(userId) && checkExist(postId)) {
        res.status(200).json(await db.query(`DELETE FROM post_likes WHERE user_id=${userId} AND post_id=${postId}`));
        return ;
    }
    res.status(400).json({});
});
module.exports =router;