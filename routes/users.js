var express = require('express');
var router = express.Router();
var database = require("../modules/db.js");
var mailer = require("../modules/mailer");
var bcrypt = require('bcrypt');
var saltRounds=10;
function isEmptyObject(obj) {
  return !Object.keys(obj).length;
}
function checkExist(a) {
  return typeof a !== "undefined" && a !== null;
}
async function getUserByLog(email) {
  return await db.query(`SELECT * FROM users WHERE email='${email}'`).catch(err => {throw err});
}

let db= database.db;
/* GET users listing. */
router.get('/auth',async(req,res)=> {
  let data=req.query;

  if (checkExist(data.userId) && checkExist(data.userHash)) {
    let userId=data.userId;
    let userHash=data.userHash;
    res.status(200).json(await db.query(`SELECT * FROM users WHERE id=${userId} AND pass_hash='${userHash}'`));
    return ;
  }
  res.status(400).json({});
});
router.get('/', async(req, res)=>{
  let data=req.query;

  if (checkExist(data.userId) && checkExist(data.userHash)) {
    let userId=data.userId;
    let userHash=data.userHash;
    res.status(200).json(await db.query(`SELECT * FROM users WHERE id=${userId} AND pass_hash=${userHash}`));
    return ;
  }
  let users = await db.query(`SELECT * FROM users`);
  res.status(200).json(users);
});
router.post('/verify',async (req,res) => {
  if (!checkExist(req.body.data.email) || !checkExist(req.body.data.verifyData)) {
    res.status(400).json({});
    return ;
  }
  let email = req.body.data.email;
  let verifyData= req.body.data.verifyData;

  let user = await getUserByLog(email).catch(err => {throw err});
  if (isEmptyObject(user)) {
    res.status(400).json({});
    return 0;
  }
  let userId = user[0].id;
  let ver = await db.query(`SELECT * FROM user_verification WHERE user_id ='${userId}'`).catch(err => {throw err});
  let verData = ver[0].verifyData;
  if (isEmptyObject(ver)) {
    res.status(400).json({});
    return ;
  }
  console.log(ver);
  console.log(verData + " " + verifyData);
  let verId= ver[0].id;

  if (verData===verifyData) {                  // Получили хэш из базы данных, сверяем. Если одинаковы - подверждаем почту
    await db.query (`UPDATE users SET user_type='VERIFIED' WHERE email='${email}'`).catch(err => {throw err});
    await db.query (`DELETE FROM user_verification WHERE id='${verId}'`).catch(err => {throw err});
    console.log(`verifying ${email} is done`);
    res.status(200).json( {
      verify : "verified",
      id: userId,
      hash: user[0].pass_hash
    });
    return ;
  }
  res.status(400).json({});
});
router.post('/register',async(req,res) => {
  let data=req.body.data;
  console.log(data);
  if (checkExist(data.password) && checkExist(data.email)) {
    pass = String(data.password);
    email = String(data.email);
    salt = await bcrypt.genSalt(saltRounds);
    hash = await bcrypt.hash(pass,salt);
    /* Получили данные и создали хэш с солью */
    let ifExist = await getUserByLog(email).catch(err => {throw err});

    if (isEmptyObject(ifExist)) {
      let user=await db.query(`
                    INSERT INTO users (email,pass_hash,pass_salt,user_type) 
                    VALUES ( '${email}' ,'${hash}','${salt}','UNVERIFIED')`).catch(err => {throw err});

      let userId=user.insertId;
      let regData=await bcrypt.genSalt(saltRounds);

      let fin=await db.query(`INSERT INTO user_verification (user_id,verifyData) VALUES ('${userId}','${regData}')`);

      let mailAns = await mailer.sendMessage(email,
          "Подверждение почты в simul",
          `Пожалуйста, подвердите ваш аккаунт по ссылке: <a href='${database.frontUrl}/users/verify?email=${email}&verifyData=${regData}'>Ссылка</a>`);
      console.log(`registering ${email} is done`);
      res.status(200).json({
        register : "register"
      });
      return ;
    }
  }
  res.status(400);

});
router.get('/login',async(req,res)=> {
  let email= req.query.email;
  let password= req.query.password;
  let user = await getUserByLog(email).catch(err => {throw err});

  if (!isEmptyObject(user)) {
    let hash = user[0].pass_hash;
    let ifGood= await bcrypt.compare(password,hash);
    if (ifGood) {
      console.log(`login ${email} is done`);
      res.status(200).json( {
        login : "login",
        id : user[0].id,
        hash : hash
      });
    }
  }
  res.status(400);
});
module.exports = router;
