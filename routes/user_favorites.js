var express = require('express');
var router = express.Router();
var database = require("../modules/db.js");
let db= database.db;

const multer= require("multer");
const path = require('path');
function isEmptyObject(obj) {
    return !Object.keys(obj).length;
}
function checkExist(a) {
    return typeof a !== "undefined" && a !== null;
}

router.get('/',async (req,res)=> {
    let data=req.query;
    let userId=data.userId;
    if (checkExist(userId)) {

        let favorites = await db.query(`SELECT * FROM user_favorite_companies WHERE user_id=${data.userId}`);
        if (favorites.length > 0) {
            let request = `SELECT * FROM companies  WHERE`;
            for (let i = 0; i < favorites.length; ++i) {

                request += ` id=${favorites[i].company_id} `;
                if (i !== favorites.length - 1) {
                    request += ` OR`
                }
                ;
            }
            let favCompanies = await db.query(request);
            res.status(200).json(favCompanies);
            return;
        }
    }
    res.status(400).json();
});
router.post('/',async (req,res) => {
   let data=req.body;
   let userId=data.userId;
   let companyId=data.companyId;
   if (checkExist(userId) && checkExist(companyId)) {
       let isExist = await db.query(`SELECT * FROM user_favorite_companies WHERE user_id=${userId} AND company_id=${companyId}`);
       if (isEmptyObject(isExist)) {
           let ans= await db.query(`INSERT INTO user_favorite_companies (user_id , company_id) VALUES ('${userId}' , '${companyId}')`);
           res.status(200).json(ans);
       }

       return ;
   }
   res.status(400).json({});
});
router.delete('/delete',async(req,res)=> {
    let data=req.query;
    let userId=data.userId;
    let companyId=data.companyId;
    if (checkExist(userId) && checkExist(companyId)) {
     res.status(200).json(await db.query(`DELETE FROM user_favorite_companies WHERE user_id=${userId} AND company_id=${companyId}`));
        return ;
    }
    res.status(400).json({});
});
module.exports =router;