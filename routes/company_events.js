var express = require('express');
var router = express.Router();
var database = require("../modules/db.js");
var mailer = require("../modules/mailer");
let db= database.db;

const multer= require("multer");
const path = require('path');
function isEmptyObject(obj) {
    return !Object.keys(obj).length;
}
function checkExist(a) {
    return typeof a !== "undefined" && a !== null;
}
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'C:/Projects_Hackaton/BitUp14-front/assets/events')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname) //Appending extension
    }
});
const fileUpload=multer({ storage : storage});
router.get("/",async (req,res)=> {

    let data=req.query;
    let offset=0;
    if (checkExist(data.offset)) offset=data.offset;
    let limit=50;
    if ( checkExist(data.limit)) limit=data.limit;
    console.log(data);
    if (checkExist(data.event_id)) {
        let event = await db.query(`SELECT * FROM company_events WHERE id=${data.event_id}`);
        res.status(200).json(event);
        return ;
    }
    if (checkExist(data.companyId)) {
        let posts=await   db.query(`SELECT * FROM company_events WHERE company_id=${data.companyId} LIMIT ${offset} , ${limit}   `);
        res.status(200).json(posts);
        return ;
    }
    let posts= await db.query(`SELECT * FROM company_events LIMIT ${offset} , ${limit}   `);
    res.status(200).json(posts);
    return ;
});
router.post("/", fileUpload.single(    'image'
),async (req,res)=> {
    let imageName=req.file.filename;
    let body =req.body;
    let companyId=body.company_id;
// TODO: check if user has company    let companyHash=body.company_hash;
    let eventTitle=body.title;
    let eventText=body.text;
    let needs=body.needs;
    let from= new Date(Date.parse(body.date_from));
    let to= new Date( Date.parse(body.date_to));
    console.log(from);
    console.log(to);
    let dateFrom=from.getFullYear() + "-" + (from.getMonth() + 1) + "-" + from.getDate();
    let dateTo=to.getFullYear() + "-" + (to.getMonth() + 1) + "-" + to.getDate();

    console.log(body);
    let insert=await db.query(`INSERT INTO company_events (company_id , title , text , image , date_start , date_end) 
    VALUES ('${companyId}','${eventTitle}','${eventText}','${imageName}' , '${dateFrom}' , '${dateTo}')`);
    let eventId=insert.insertId;
    let insertNeeds='INSERT INTO company_event_needs (company_event_id , needing_id) VALUES ';
    for (let i=0; i<needs.length;++i) {
        let users=await db.query(`SELECT * FROM company_solve_needs WHERE needing_id=${needs[i]}`);
        let needing =await db.query(`SELECT * FROM needs WHERE id=${needs[i]}`);
        let str=needing[0].needing;
        for (let j=0;j<users.length;++j) {
            console.log(users[j]);
            let company =await db.query(`SELECT * FROM companies WHERE ID =${users[j].company_id}`);
            console.log(company);
            mailer.sendMessage(company[0].email,`Появилось мероприятие, ему нужна помощь по теме '${str}'`);
        }
        insertNeeds+= `(${eventId} , ${needs[i]})`;
        if (i!==needs.length-1) {
            insertNeeds+=', ';
        }
    }
    await db.query(insertNeeds);
    res.status(200).json(insert);
});
module.exports=router;