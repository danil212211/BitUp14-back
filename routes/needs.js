var express = require('express');
var router = express.Router();
var database = require("../modules/db.js");
let db= database.db;

const multer= require("multer");
const path = require('path');
function isEmptyObject(obj) {
    return !Object.keys(obj).length;
}
function checkExist(a) {
    return typeof a !== "undefined" && a !== null;
}
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'C:/Projects_Hackaton/BitUp14-front/assets/needs')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname) //Appending extension
    }
});
router.get('/',async (req,res)=> {
   let needs=await db.query(`SELECT * FROM needs`);
   res.status(200).json(needs);
});

module.exports = router;