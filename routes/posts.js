var express = require('express');
var router = express.Router();
var database = require("../modules/db.js");
let db= database.db;

const multer= require("multer");
const path = require('path');
function isEmptyObject(obj) {
    return !Object.keys(obj).length;
}
function checkExist(a) {
    return typeof a !== "undefined" && a !== null;
}
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'C:/Projects_Hackaton/BitUp14-front/assets/posts')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname) //Appending extension
    }
});

const fileUpload=multer({ storage : storage});
router.get("/",async (req,res)=> {

    let data=req.query;
    let offset=0;
    if (checkExist(data.offset)) offset=data.offset;
    let limit=50;
    if ( checkExist(data.limit)) limit=data.limit;
    if (checkExist(data.companyId)) {
        let posts=await   db.query(`SELECT * FROM posts WHERE company_id=${data.companyId} LIMIT ${offset} , ${limit}   `);
        res.status(200).json(posts);
        return ;
    }
    if (checkExist(data.post_id)) {
        let post = await db.query(`SELECT * FROM posts WHERE id=${data.post_id}`);
        res.status(200).json(post);
        return ;
    }
    if (checkExist(data.userId)) {
        console.log("withuser");
        let userId=data.userId;
        let posts= await db.query(`SELECT * FROM posts LIMIT ${offset} , ${limit}   `);
        let arr=[];
        for (let i=0;i<posts.length;++i) {
            let likes=await db.query(`SELECT COUNT(id) FROM post_likes WHERE post_id=${posts[i].id}`);
            let isLike=await db.query(`SELECT * FROM post_likes WHERE post_id=${posts[i].id} AND user_id=${userId}`);
            console.log("check likes");
            console.log(likes);
            console.log(likes[0]['COUNT(id)']);
            console.log(isLike);
            if (checkExist(isLike) && !isEmptyObject(isLike)) {
                isLike=true;
            } else isLike=false;

            arr.push( {
                post : posts[i],
                likes : likes[0]['COUNT(id)'],
                isLike : isLike
            });
            console.log("ok Cool");
        }
        res.status(200).json(arr);
        return ;
    }
    let posts= await db.query(`SELECT * FROM posts LIMIT ${offset} , ${limit}   `);
    let arr=[];
    for (let i=0;i<posts.length;++i) {
        let likes=await db.query(`SELECT COUNT(id) FROM post_likes WHERE post_id=${posts[i].id}`);
        arr.push( {
            post : posts[i],
            likes : likes[0]['COUNT(id)'],
        });
    }
    res.status(200).json(arr);
    return ;
});
router.post("/", fileUpload.single('image'),async (req,res)=> {
    console.log(req.file);
    let imageName=req.file.filename;
    let body =req.body;
    let companyId=body.company_id;
//    let companyHash=body.company_hash;
    let postTitle=body.title;
    let postText=body.text;
    let insert=await db.query(`INSERT INTO posts (company_id , title , text , image) VALUES ('${companyId}','${postTitle}','${postText}','${imageName}')`);
    console.log(req);
    res.status(200).json({});
});
module.exports=router;