var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require("cors");

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var postRouter  = require('./routes/posts');
var eventRouter = require('./routes/company_events');
var companyRouter = require('./routes/companies');
var chatRouter = require('./routes/chat');
var connectionRouter  =require('./routes/connections');
var eventNeedsRouter = require('./routes/event_needs');
var postLikesRouter = require('./routes/post_likes');
var achievRouter =require('./routes/achievements');
var favoritesRouter = require('./routes/user_favorites');
var needsRouter = require('./routes/needs');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/companies/posts',postRouter);
app.use('/companies',companyRouter);
app.use('/companies/events',eventRouter);
app.use('/chat',chatRouter);
app.use('/users/achievements',achievRouter);
app.use('/companies/needs',needsRouter);
app.use('/companies/event/needs',eventNeedsRouter);
app.use('/companies/connections',connectionRouter);
app.use('/users/favorites',favoritesRouter);
app.use('/posts/likes',postLikesRouter)
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
